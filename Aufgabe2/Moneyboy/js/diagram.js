var host = 'http://141.22.84.98';

function requestData(containerType, url, currentData)
{
    $.ajax({
        url: url,
        datatype: "json",
        success: function(data)
        {
            console.log("Loading successful: " + url);
            $('#container' + containerType).highcharts().series[0].setData(data);

            // Reset Diagram
            if (data.length == 0) {
                requestData(containerType, host + '/php/init.php?container=' + containerType.toLowerCase(), currentData);

            } else {
                currentData.data = data;
            }
        },
        error: function() {
            console.log("Request failed: " + url);
        }
    });
}

var diagram = function (containerType) {
    var currentData = {data : ""};

    $('#container' + containerType).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            events: {
                load: function() {
                    requestData(containerType, host + '/php/init.php?container=' + containerType.toLowerCase(), currentData);
                }
            }
        },
        title: {
            text: containerType
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                point: {
                    events: {
                        click: function () {
                            var url = host + '/php/request.php?category=' + containerType.toLowerCase() +
                                '&value=' + encodeURIComponent(currentData.data[this.x].name);
                            console.log("Data:" + currentData);
                            console.log("Value: " + encodeURIComponent(currentData.data[this.x].name));
                            console.log("Url:" + url);
                            requestData(containerType, url, currentData);
                        }
                    }
                }
            }
        },
        series: [{
            name: containerType
        }]
    });
};

$(function () {
   diagram('Type');
   diagram('Currency');
   diagram('Region');
   diagram('Branch');
});