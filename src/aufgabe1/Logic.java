package aufgabe1;

import org.jfree.data.xy.DefaultXYDataset;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by goat on 21.10.15.
 */
public class Logic {

    /**
     * Simulates a coin toss.
     * @return true if won (> 0.5), false (<0.5)
     */
    private static boolean coinToss(){
        return Math.random() > 0.5;
    }

    /**
     * Generates reports for 2 Players, cointossing N times
     * @param n number of cointosses
     * @return reports as map
     */
    public static Map<Player, Evalsheet> nToss(int n){
        final Map<Player, Evalsheet> map = new HashMap<>();
        final List<Boolean> player1res = new LinkedList<>();
        final List<Boolean> player2res = new LinkedList<>();
        int sumPlayer1 = 0;
        int sumPlayer2 = 0;

        for(int i = 0; i < n; i++){
            boolean tossres = coinToss();
            // Toss for player 1
            player1res.add(tossres);
            player2res.add(!tossres);

            if(tossres){
                sumPlayer1++;
                sumPlayer2--;
            }else{
                sumPlayer1--;
                sumPlayer2++;
            }
            map.put(Player.ONE, new Evalsheet(player1res, sumPlayer1));
            map.put(Player.TWO, new Evalsheet(player2res, sumPlayer2));
        }
        return map;
    }

    public static DefaultXYDataset generateDatasetFromMap(Map<Player, Evalsheet> map){
        DefaultXYDataset ds = new DefaultXYDataset();
        Evalsheet evalP1 = map.get(Player.ONE);
        Evalsheet evalP2 = map.get(Player.TWO);
        // magic numbers ftw...!
        double[][] dataP1 = new double[2][evalP1.tossCount()];
        double[][] dataP2 = new double[2][evalP1.tossCount()];

        int moneyP1 = 1, moneyP2 = 0;


        for(int i = 0; i < evalP1.tossCount(); i++){
            dataP1[0][i] = i +1;
            dataP2[0][i] = i +1;
            dataP1[1][i] = evalP1.getTosses().get(i)? moneyP1++ : moneyP1--;
            dataP2[1][i] = evalP2.getTosses().get(i)? moneyP2++ : moneyP2--;
        }

        ds.addSeries(Player.ONE.toString(), dataP1);
        ds.addSeries(Player.TWO.toString(), dataP2);
        return ds;
    }

}
