package aufgabe1;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.DefaultXYDataset;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

/**
 * Created by goat on 21.10.15.
 */
public class Gui extends JFrame {
    private final JPanel menu;
    private final JPanel analysis;
    private final JPanel labelStuff;

    private final JSplitPane splitPane;
    private final JButton start;
    private final ChartPanel chartPanel;

    private final JLabel label_n;
    private final JLabel label_p1_expected;
    private final JLabel label_p2_expected;
    private final JLabel label_p1_actual;
    private final JLabel label_p2_actual;

    private final JTextField input;
    private final JTextField text_p1_expected;
    private final JTextField text_p2_expected;
    private final JTextField text_p1_actual;
    private final JTextField text_p2_actual;

    public Gui() {
        super("Awesome Aufgabe 1");

        labelStuff = new JPanel(new GridLayout(2, 4, 10, 2));
        label_n = new JLabel("Inert N:");
        label_p1_expected = new JLabel("Player 1 expected:");
        label_p2_expected = new JLabel("Player 2 expected:");
        label_p1_actual = new JLabel("Player 1 actual:");
        label_p2_actual = new JLabel("Player 2 actual:");

        text_p1_expected = new JTextField(0);
        text_p1_expected.setEditable(false);
        text_p2_expected = new JTextField(0);
        text_p2_expected.setEditable(false);
        text_p1_actual = new JTextField(0);
        text_p1_actual.setEditable(false);
        text_p2_actual = new JTextField(0);
        text_p2_actual.setEditable(false);

        labelStuff.add(label_p1_expected);
        labelStuff.add(text_p1_expected);
        labelStuff.add(label_p2_expected);
        labelStuff.add(text_p2_expected);
        labelStuff.add(label_p1_actual);
        labelStuff.add(text_p1_actual);
        labelStuff.add(label_p2_actual);
        labelStuff.add(text_p2_actual);

        input = new JTextField();
        input.setPreferredSize(new Dimension(50, 20));

        start = new JButton("Magic!");
        start.addActionListener((e) -> updateData());

        menu = new JPanel();
        menu.add(label_n);
        menu.add(input);
        menu.add(start);
        analysis = new JPanel();

        JFreeChart chart =
                ChartFactory.createXYLineChart("Simulation Result",
                        "Number Of Tosses", "Result", null, PlotOrientation.VERTICAL, true, true,
                        false);

        chartPanel = new ChartPanel(chart);
        analysis.add(chartPanel);
        analysis.add(labelStuff);

        splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, menu, analysis);
        splitPane.setEnabled(false);
        splitPane.setDividerSize(1);
        this.add(splitPane);
        this.setSize(new Dimension(680, 600));
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void updateData() {
        int in_N = 0;
        try {
            in_N = Integer.parseInt(input.getText());
        }catch(NumberFormatException e){
            System.out.println("lulululululululuu");
        }finally {
            if (in_N <= 0) {
                JOptionPane.showMessageDialog(this, "Check input dawg!", "Check input more dawg!", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        Map<Player, Evalsheet> playerEvalsheetMap = Logic.nToss(in_N);
        DefaultXYDataset data4chart = Logic.generateDatasetFromMap(playerEvalsheetMap);
        JFreeChart newChart = ChartFactory.createXYLineChart("Simulation Result",
                "Number Of Tosses", "Result", data4chart, PlotOrientation.VERTICAL, true, true,
                false);
        chartPanel.setChart(newChart);

        text_p1_expected.setText("0");
        text_p2_expected.setText("0");
        text_p1_actual.setText("" + playerEvalsheetMap.get(Player.ONE).getSum());
        text_p2_actual.setText("" + playerEvalsheetMap.get(Player.TWO).getSum());
    }

}
