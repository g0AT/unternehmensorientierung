package aufgabe1;

import java.util.List;

/**
 * Created by goat on 21.10.15.
 */
public class Evalsheet {
    private List<Boolean> tosses;
    private int sum;

    public Evalsheet(List<Boolean> tosses, int sum){
        this.tosses = tosses;
        this.sum = sum;
    }

    public List<Boolean> getTosses() {
        return tosses;
    }

    public int getSum() {
        return sum;
    }

    @Override
    public String toString() {
        return "Evalsheet{" +
                "tosses=" + tosses +
                ", sum=" + sum +
                '}';
    }

    public int tossCount(){
        return tosses.size();
    }

}
